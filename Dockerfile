FROM node

RUN mkdir /app
COPY ./flatris_new/package.json /app/package.json

WORKDIR /app
RUN yarn install

COPY ./flatris_new /app
RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
